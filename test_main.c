#include "functions.c"

#include <stdio.h>

int             main(int ac, char *av[])
{
        char    *ret_str;

        if (ac == 3)
        {
                ret_str = multiply(av[1], av[2]);
                printf("\nret was : %s\n", ret_str);
                free(ret_str);
        }
        return (0);
}
