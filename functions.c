#include <string.h>
#include <stdlib.h>

char	*char_num_to_rev_num_num(char const * num_str, size_t len)
{
	char	*ret_str;
	size_t	i;

	ret_str = 0;
	if (len && (ret_str = malloc(len + 1)))
	{
		ret_str[len] = '\0';
		i = 0;
		if (num_str)
			while (i < len)
			{
				ret_str[i] = num_str[i] - '0';
				i++;
			}
	}
	return (ret_str);
}

void	num_num_to_char_num(char * nums, size_t len)
{
	while (len && !nums[--len])
		;
	while (len)
		nums[len--] += '0';
	nums[0] += '0';
}

void	reverse(char *rev_num)
{
	size_t	j;
	size_t	i;
	char	tmp;

	i = 0;
	while (rev_num[i])
		i++;
	j = 0;
	while (j < i / 2)
	{
		tmp = rev_num[j];
		rev_num[j] = rev_num[i - 1 - j];
		rev_num[i - 1 - j] = tmp;
		j++;
	}
}

char	*multiply(char *a, char *b)
{
	const size_t	a_len = strlen(a);
	const size_t	b_len = strlen(b);

	char			*wk_sum;
	char			*a_cpy;
	char			*b_cpy;
	size_t			max_10_pow;
	size_t			i;
	size_t			j;

	max_10_pow = a_len + b_len;
	if ((wk_sum = calloc(1, max_10_pow + 1)) &&\
		(a_cpy = char_num_to_rev_num_num(a, a_len)) &&\
		(b_cpy = char_num_to_rev_num_num(b, b_len)))
	{
		i = 0;
		while (i < b_len)
		{
			j = 0;
			while (j < a_len)
			{
				wk_sum[i + j] += a_cpy[a_len - 1 - j] * b_cpy[b_len - 1 - i];
				if (wk_sum[i + j] > 10)
				{
					wk_sum[i + j + 1] += wk_sum[i + j] / 10;
					wk_sum[i + j] %= 10;
				}
				j++;
			 }
			i++;
		}
		num_num_to_char_num(wk_sum, max_10_pow);
		reverse(wk_sum);
	}
	if (a_cpy)
	{
		free(a_cpy);
		if (b_cpy)
			free(b_cpy);
	}
	return (wk_sum);
}
